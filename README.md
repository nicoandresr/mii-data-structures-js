# OVERVIEW
Personal site for learn and practicje about Data Structures in Javascript,

## ARCHITECTURE
- parcel-bundler
- tailwindcss cdn
- react@next
- jest
- eslint
- husky
- babel

## TOPICS
The topics that I'm looking for cover in this project are:
- Graphs
## CI-CD
This project have a basic configuration for CI-CD in gitlab pages, with the following steps:
- Test
- Lint
- Build
- Deploy

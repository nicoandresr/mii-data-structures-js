import * as React from 'react'
import { renderToStaticMarkup as render } from 'react-dom/server'

import App from './app'

test('App', () => {
  expect(render(<App />)).toMatchSnapshot()
})
